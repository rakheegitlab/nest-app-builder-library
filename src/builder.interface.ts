import { INestApplication } from '@nestjs/common';

export interface IBuilder {
  build(app: INestApplication, options: object);
}
