import { BuilderRegistry } from '../../src/builder.registry';
import { DemoBuilder } from '../mocks/demoBuilder';
import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';

describe('nest-app-builder library', () => {

  let builder: BuilderRegistry;

  beforeAll(async () => {
    builder = BuilderRegistry.instance();
  });

  it('check singleton', async () => {
    expect(builder).toEqual(BuilderRegistry.instance());
  });

  it('getAllBuilder test', async () => {
    expect(await builder.getAllBuilders().length).toEqual(2);
  });

  it('registerbuilder test', async () => {
    const demoBuilder = DemoBuilder.instance();
    builder.registerBuilder(demoBuilder);
    expect(await builder.getAllBuilders().length).toEqual(3);
  });

  it('buildAll test', async() => {
    const app = await NestFactory.create(AppModule);
    expect(await builder.buildAll(app).length).toEqual(3);
  })

});
