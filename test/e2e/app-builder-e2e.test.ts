import * as request from 'supertest';
import { TestApp } from '../main';

describe('AppController (e2e)', () => {
  let app;
  const testApp = TestApp.instance(app);

  beforeAll(async () => {
    app = await testApp.bootstrap();
  });

  test('api end point check ', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect({ accepted: true });
  });

  test('swagger end point check ', () => {
    return request(app.getHttpServer())
      .get('/api')
      .expect(301)
      .expect({});
  });

  afterAll(async () => {
    await testApp.closeApp();
  });

});
