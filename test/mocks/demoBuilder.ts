import { IBuilder } from '../../src/builder.interface';
import { INestApplication } from '@nestjs/common';

export class DemoBuilder implements IBuilder {

  public static instance(): IBuilder {
    if (this.thisObject) {
      return this.thisObject;
    }
    this.thisObject = new DemoBuilder();
    return this.thisObject;
  }

  private static thisObject: DemoBuilder;
  private constructor() { }

  public build(app: INestApplication) {
    return app;
  }
}
