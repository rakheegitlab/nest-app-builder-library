import { INestApplication } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { BuilderRegistry } from '.././src/builder.registry';
import { AppModule } from './app.module';

export class TestApp {

  private app: INestApplication;

  private constructor(app) {
    this.app = app;
  }

  public static instance(app): TestApp {
    if (this.thisObject) {
      return this.thisObject;
    }
    this.thisObject = new TestApp(app);
    return this.thisObject;
  }

  private static thisObject: TestApp;

  public async bootstrap() {
    this.app = await NestFactory.create(AppModule);

    const swaggerOptions = {
      title: 'NestJs Example',
      description: 'NestJs Api Description',
      version: '1.0',
      tag: 'NestJsProject'
    }
    const options = { swaggerOptions }

    const builderRegistry: BuilderRegistry = BuilderRegistry.instance();
    await builderRegistry.buildAll(this.app, options);

    await this.app.listen(3000);
    return this.app
  }

  public async closeApp() {
    return await this.app.close();
  }
}
